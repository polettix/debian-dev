#!/bin/sh
exec 1>&2  # send everything to log

srcdir="$(cat DIBS_DIR_SRC)"
user="$1"
userhome="$2"

apt-get update
apt-get install -y build-essential perl libperl-dev git
apt-get clean

cp "$srcdir"/suexec /

useradd -m -d "$userhome" -U "$user"

printf '%s\n' '. $HOME/.bashrc' >"$userhome/.profile"
cat >"$userhome/.bashrc" <<'END'
alias dir='ls -l'
END
chown "$user:$user" "$userhome/.profile" "$userhome/.bashrc"

cp "$srcdir"/su-exec/su-exec-static /usr/bin/su-exec
chmod +x /usr/bin/su-exec # be on the safe side
