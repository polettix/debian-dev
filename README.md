# debian-dev

This repository should help you to build a development-oriented [Docker][]
container image, using [dibs][].

It includes a snapshot of [su-exec][] by [Natanael Copa][] as of
2020-03-05, inside the `su-exec` sub-directory, along with a stripped,
statically-compiled version of the binary.

Except for the contents of the `su-exec` sub-directory, which contains a
`src/LICENSE` file related to it, the rest is licensed according to the
Apache License 2.0 (see file `LICENSE` in the project's root directory):

>  Copyright 2020 by Flavio Poletti
>
>  Licensed under the Apache License, Version 2.0 (the "License");
>  you may not use this file except in compliance with the License.
>  You may obtain a copy of the License at
>
>      http://www.apache.org/licenses/LICENSE-2.0
>
>  Unless required by applicable law or agreed to in writing, software
>  distributed under the License is distributed on an "AS IS" BASIS,
>  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
>  See the License for the specific language governing permissions and
>  limitations under the License.

Generated images are available in the repository's [container registry][].

[Docker]: https://www.docker.com/
[dibs]: http://blog.polettix.it/hi-from-dibs/
[su-exec]: https://github.com/ncopa/su-exec
[Natanael Copa]: https://github.com/ncopa
[container registry]: https://gitlab.com/polettix/debian-dev/container_registry
